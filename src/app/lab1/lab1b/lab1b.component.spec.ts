import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lab1bComponent } from './lab1b.component';

describe('Lab1bComponent', () => {
  let component: Lab1bComponent;
  let fixture: ComponentFixture<Lab1bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab1bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab1bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
