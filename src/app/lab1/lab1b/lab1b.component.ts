import { Component, OnInit } from '@angular/core';
import { Question } from '../question-block/question-block.component';

@Component({
  selector: 'ai-lab1b',
  templateUrl: './lab1b.component.html',
  styleUrls: ['./lab1b.component.scss']
})
export class Lab1bComponent implements OnInit {

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Level';
  showYAxisLabel = true;
  yAxisLabel = 'Score';
  view = [900, 600]

  colorScheme = {
    domain: ['#FF6F00']
  };

  blocks: Question[][] = [
    [
      { title: "Переживаєте за успіх в роботі", options: ["сильно", "не дуже", "спокійний"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Прагнете досягти швидко результату", options: ["поступово", "якомога швидше", "дуже"], values: [2, 3, 5], selectedAnswer: 0 },
      { title: "Легко попадаєте в тупик при проблемах в роботі", options: ["неодмінно", "поступово", "зрідка"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи   потрібен чіткий алгоритм для вирішення задач", options: ["так", "в окремих випадках", "не потрібен"], values: [5, 3, 2], selectedAnswer: 0 },
    ],
    [
      { title: "Чи використовуєте власний досвід при вирішенні задач", options: ["зрідка", "частково", 'ні'], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи користуєтесь фіксованими правилами  для вирішення задач", options: ["так", "в окремих випадках", "не потрібні"], values: [2, 3, 5], selectedAnswer: 0 },
      { title: "Чи відчуваєте ви загальний контекст вирішення задачі", options: ["так", "частково", "в окремих випадках"], values: [2, 3, 5], selectedAnswer: 0 },
    ],
    [
      { title: "Чи можете ви побудувати модель вирішуваної задачі", options: ["так", "не повністю", "в окремих випадках"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи вистачає вам ініціативи при вирішенні задач", options: ["так", "зрідка", "потрібне натхнення"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи можете вирішувати проблеми, з якими ще не стикались", options: ["так", "в окремих випадках", "ні"], values: [2, 3, 5], selectedAnswer: 0 },
    ],
    [
      { title: "Чи  необхідний вам весь контекст задачі ", options: ["так", "в окремих деталях", "в загальному"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи переглядаєте ви свої наміри до вирішення задачі", options: ["так", "зрідка", "коли є потреба"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи здатні  ви  навчатись у інших ", options: ["так", "зрідка", "коли є потреба"], values: [5, 3, 2], selectedAnswer: 0 },
    ],
    [
      { title: "Чи обираєте ви нові методи своєї роботи", options: ["так", "вибірково", "вистачає досвіду"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи допомагає власна інтуїція при вирішенні задач", options: ["так", "частково", "при емоційному напруженні"], values: [5, 3, 2], selectedAnswer: 0 },
      { title: "Чи застовуєте рішення задач за аналогією", options: ["часто", "зрідка", "тільки власний варіант"], values: [5, 3, 2], selectedAnswer: 0 },
    ]
  ]

  metas: any[] = [
    {
      level: "Novice",
      total: 0,
      data: []
    }, {
      level: "Advanced beginner",
      total: 0,
      data: []
    },
    {
      level: "Competent",
      total: 0,
      data: []
    },
    {
      level: "Proficient",
      total: 0,
      data: []
    },
    {
      level: "Expert",
      total: 0,
      data: []
    }
  ]

  typeToScore = []
  constructor() { }

  ngOnInit() {
  }


  restart() {
    this.blocks.forEach(b => {
      b.forEach(q => {
        q.selectedAnswer = 0
      });
    });
    this.metas.forEach((m) => {
      m.total = 0
    })
  }

  chartEnabled = true
  results() {
    this.chartEnabled = !this.chartEnabled
    this.update()
  }


  update() {

    for (let i = 0; i < this.blocks.length; i++) {
      const b = this.blocks[i];
      this.metas[i].data = []
      for (let j = 0; j < b.length; j++) {
        this.metas[i].data.push({ name: j, value: b[j].selectedAnswer })
      }
    }
    this.typeToScore = []
    this.metas.forEach((m) => {
      this.typeToScore.push({ name: m.level, value: m.total })
    })
    console.log(this.typeToScore);

  }
}
