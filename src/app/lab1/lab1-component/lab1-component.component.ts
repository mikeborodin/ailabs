import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ai-lab1-component',
  templateUrl: './lab1-component.component.html',
  styleUrls: ['./lab1-component.component.scss']
})
export class Lab1Component implements OnInit {

  constructor() { }
  results = ''
  score = 0
  motion: number
  calm: number
  certain: number
  notCertain: number



  ngOnInit() {
  }

  cumputeResults() {
    this.score = this.calm + this.motion + this.certain + this.notCertain
    this.results = ''
    if (this.score > 8 && this.score <= 13) {
      this.results = ' Ваша поведінка залежить передусім від тих, хто вас оточує. Ви легко занепадаєте духом, легко «вибухаєте», неохоче робите те, що вам не до вподоби. На вас справляє сильний вплив оточення, мікроклімат у колективі. Отже, ви не є господарем своїх рішень. Ви надто емоційні, і ваші вчинки залежать здебільшого від вашого настрою'
    }
    if (this.score >= 14 && this.score <= 20) {
      this.results = 'Хоча й «пливете за течією», але намагаєтесь знайти свій стиль. Здатні до критичного аналізу. Коли здоровий глузд підказує, що ваша позиція хибна, ви цілком спроможні від неї відмовитись.'
    }
    if (this.score >= 21 && this.score <= 27) {
      this.results = 'Вважаєте себе непогрішимим. І все ж, підпадаєте під сторонній вплив. Шукаєте золоту середину між власними переконаннями й тими вимогами, які ставить життя. Допомагає те, що ви інстинктивно обираєте правильний шлях.'
    }
    if (this.score >= 28 && this.score <= 34) {
      this.results = 'Вам нелегко відмовитись від своїх принципів. Що більше хтось намагається на вас тиснути, то сильніший опір ви чините. Однак за впертістю приховується не стільки впевненість, скільки страх пошитися в дурні.'
    }

    if (this.score >= 35 && this.score <= 40) {
      this.results = 'Якщо вже щось спаде вам на думку, то ніхто вас не переконає у протилежному. Попри все йдете до своєї мети. Але той , хто вас добре знає, може вміло та непомітно скеровувати ваші вчинки. Тож чи не варто проявляти менше впертості і більше кмітливості?.'
    }
  }

  reset() {
    this.calm = 0
    this.certain = 0
    this.notCertain = 0
    this.motion = 0
    this.score = 0
    this.results = ''
  }
}
