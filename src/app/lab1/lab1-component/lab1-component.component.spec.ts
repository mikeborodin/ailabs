import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lab1ComponentComponent } from './lab1-component.component';

describe('Lab1ComponentComponent', () => {
  let component: Lab1ComponentComponent;
  let fixture: ComponentFixture<Lab1ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab1ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
