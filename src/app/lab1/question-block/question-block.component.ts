import { Component,EventEmitter, OnInit, Input, Output } from '@angular/core';

export interface Question {
  title: string,
  options: string[],
  values: number[],
  selectedAnswer: number,
  total?: number
}



@Component({
  selector: 'ai-question-block',
  templateUrl: './question-block.component.html',
  styleUrls: ['./question-block.component.scss']
})
export class QuestionBlockComponent implements OnInit {

  @Input() questions: Question[] = []
  @Input() meta: any
  @Input() chartEnabled: boolean = false

  // chart
  data = []
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Question';
  showYAxisLabel = true;
  yAxisLabel = 'Value';
  view = [600,300]
  
  colorScheme = {
    domain: ['#2979FF']
  };
  @Output() onBarChange:EventEmitter<any> = new EventEmitter()

  constructor() { 
  }

  ngOnInit() {
  }

  updateTotal(newSelected, qi, i) {
    this.questions[qi].selectedAnswer = newSelected

    let t = 0
    this.questions.forEach(q => {
      t += q.selectedAnswer
    });
    this.meta.total = t
    this.onBarChange.emit()
  }
}
