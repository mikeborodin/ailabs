import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Lab1Component } from './lab1/lab1-component/lab1-component.component';
import { MainComponent } from './main/main.component';
import { Lab1bComponent } from './lab1/lab1b/lab1b.component';

const routes: Routes = [
  { path: "", pathMatch: 'full', redirectTo: 'home' },
  { path: "main", component: MainComponent },
  { path: "lab1", component: Lab1Component },
  { path: "lab2", component: Lab1bComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
